# 简介

用户安装 flume，必须先安装 jdk 的环境，修改对应 JAVA_HOME 变量即可。

## 使用

```bash
bash install_flume.sh
```

会创建一个默认的配置文件和启动文件，根据实际情况进行修改即可。
