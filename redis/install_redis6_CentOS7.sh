#!/bin/bash
# author : Jalright
# update : 2021-01-27 add yum install gcc
# update : 2021-11-11 fix scl enable devtoolset-9

redis_version="6.2.6"

install_path="/opt/redis"

cd $HOME

wget -c -t 0 -T 12000 https://download.redis.io/releases/redis-${redis_version}.tar.gz

tar zxvf redis-${redis_version}.tar.gz

cd redis-${redis_version}

# 需要高版本的gcc，可以直接使用yum进行安装
yum -y install centos-release-scl

yum -y install devtoolset-9-gcc devtoolset-9-gcc-c++ devtoolset-9-binutils

scl enable devtoolset-9 make

scl enable devtoolset-9 make PREFIX=${install_path} install