#!/bin/bash

function make_bin() {
    REDIS_VERSION="6.2.6"
    IPATH="./redis-bin"
    IPATH=$(readlink -f $IPATH)
    CURRENT_DIR=$(readlink -f ./)
    mkdir -p ${IPATH}
    cd /tmp
    wget -c -t 0 -T 12000 https://download.redis.io/releases/redis-${REDIS_VERSION}.tar.gz
    tar zxvf redis-${REDIS_VERSION}.tar.gz
    cd redis-${REDIS_VERSION}
    # 需要高版本的gcc，可以直接使用yum进行安装
    yum -y install centos-release-scl
    yum -y install devtoolset-9-gcc devtoolset-9-gcc-c++ devtoolset-9-binutils
    scl enable devtoolset-9 make
    scl enable devtoolset-9 "make PREFIX=${IPATH} install"
    \cp -v redis.conf sentinel.conf ${IPATH}
    echo $REDIS_VERSION >>${IPATH}/version.txt
    cd ${IPATH}
    tar zcvf $CURRENT_DIR/redis_bin.tar.gz *
}

function install_redis() {
    INSTALL_PATH=$1
    if [[ -f redis_bin.tar.gz && "${INSTALL_PATH}" != "" ]]; then
        mkdir -p ${INSTALL_PATH}
        tar zxvf redis_bin.tar.gz -C ${INSTALL_PATH}
    else
        echo "二进制包不存在，请先执行make_bin"
    fi
}

function config_redis() {
    server_bin=$1
    config_file=$2
    port=$3
    data_dir=$4
    echo "SERVER_BIN:$server_bin"
    echo "CONFIGFILE:$config_file"
    echo "PORT:$port"
    echo "DATA_DIR:$data_dir"
    echo -n "确认配置（y/n）:"
    read ok
    if [ "$ok" != "y" ]; then
        echo "退出"
        return
    fi
    # create random password
    random_password=""
    arr=(a b c d e f g h i g k l m n o p q r s t u v w x y z
        A B C D E F G H I G K L M N O P Q R S T U V W X Y Z
        ! @ 0 1 2 3 4 5 6 7 8 9)
    for ((i = 0; i < 20; i++)); do
        random_password=${random_password}${arr[$RANDOM % ${#arr[@]}]}
    done

    cat >${config_file} <<EOF
bind 0.0.0.0
protected-mode yes
port ${port}
daemonize no
pidfile ${data_dir}/redis_${port}.pid
loglevel notice
logfile "${data_dir}/${port}.log"
databases 16
always-show-logo yes
dbfilename dump_${port}.rdb
dir ${data_dir}
appendonly yes
appendfilename "appendonly_${port}.aof"
appendfsync everysec
requirepass "${random_password}"
EOF

    cat >/usr/lib/systemd/system/redis.service <<EOF
[Unit]
Description=Redis
After=network.target

[Service]
#Type=forking
PIDFile=${data_dir}/redis.pid
ExecStart=${server_bin} ${config_file}
ExecReload=/bin/kill -s HUP \$MAINPID
ExecStop=/bin/kill -s QUIT \$MAINPID
PrivateTmp=true
LimitNOFILE=40960
LimitNPROC=40960
User=redis
Group=redis
[Install]
WantedBy=multi-user.target
EOF
}

case $1 in
make_bin)
    make_bin
    ;;
install)
    if [ "$2" == "" ]; then
        echo "$0 $1 INSTALL_PATH"
    else
        install_redis $2
    fi
    ;;
config)
    if [ "$2" == "" ]; then
        echo "$0 $1 SERVER_BIN CONFIG_FILE PORT DATADIR"
    elif [ "$3" == "" ]; then
        echo "$0 $1 SERVER_BIN CONFIG_FILE PORT DATADIR"
    elif [ "$4" == "" ]; then
        echo "$0 $1 SERVER_BIN CONFIG_FILE PORT DATADIR"
    elif [ "$5" == "" ]; then
        echo "$0 $1 SERVER_BIN CONFIG_FILE PORT DATADIR"
    else
        config_redis $2 $3 $4 $5
    fi
    ;;
*)
    echo "$0 [make_bin|install|config]"
    ;;
esac
