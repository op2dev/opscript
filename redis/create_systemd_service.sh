#!/bin/bash


port=7000
PIDFile="/data/redis/redis_${port}.pid"
Exec="/opt/redis/bin/redis-server"
conf="/opt/redis/etc/${port}.conf"

cat > /lib/systemd/system/redis_${port}.service << EOF
[Unit]
Description=Redis_${port}
After=network.target

[Service]
Type=forking
PIDFile=${PIDFile}
ExecStart= ${Exec}  ${conf} --supervised systemd
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s QUIT $MAINPID
LimitNOFILE=655360
PrivateTmp=true
Restart=always
Type=notify
User=redis
Group=redis
RuntimeDirectory=redis
RuntimeDirectoryMode=0755

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable redis_${port}
systemctl start redis_${port}