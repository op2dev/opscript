# 部署redis cluster集群

这个脚本需要的环境是`CentOS 7`

## 安装软件包

在各个节点安装软件包

```bash
bash ./install_soft.sh
```

## 创建配置文件

每个节点需要的配置文件，可以根据自己的需要修改脚本里面的端口，密码如果不设置就会生成随机的密码。

```bash
bash create_config.sh
```

## 创建集群

创建集群的命令，有多少个节点就写多少个，自从会自动分配。

```bash
/data/redis/bin/redis-cli --cluster create \
192.168.122.1:7001 192.168.122.1:7002 \
192.168.122.1:7003 192.168.122.1:7004 \
192.168.122.1:7005 192.168.122.1:7006 \
192.168.122.1:7007 192.168.122.1:7008 \
192.168.122.1:7009 \
--cluster-replicas 2 -a OxYLZdfP1dvBpice
```
