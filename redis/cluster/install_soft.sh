#!/bin/bash
# author : Jalright
# create : 2021-05-27 install redis software 

VERSION="6.2.6"
INSTALLPATH="/opt/redis/redis"

# gcc new version
yum -y install centos-release-scl

yum -y install devtoolset-9-gcc devtoolset-9-gcc-c++ devtoolset-9-binutils wget tar

# download file to $HOME
cd $HOME

wget -c -t 0 -T 12000 https://mirrors.huaweicloud.com/redis/redis-${VERSION}.tar.gz

tar zxvf redis-${VERSION}.tar.gz

cd redis-${VERSION}

scl enable devtoolset-9 bash

make

make PREFIX=${INSTALLPATH} install


