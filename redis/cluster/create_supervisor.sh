#!/bin/bash
REDISBIN="/opt/redis/bin/redis-server"
LOGDIR="/data/redis_cluster/logs"
CONFIGDIR="/opt/redis/cluster/"
SUPERVISORCONF="/opt/redis/supervisor"
mkdir -p $LOGDIR $SUPERVISORCONF $CONFIGDIR

PORTS=(7001 7002 7003 7004 7005 7006 7007 7008 7009)
for ((i = 0; i < ${#PORTS[@]}; i++)); do
    PORT=${PORTS[${i}]}
    cat >${SUPERVISORCONF}/supervisor_${PORT}.ini <<EOF
[program:redis_$PORT]
command=$REDISBIN  ${CONFIGDIR}/${PORT}.conf              ; the program (relative uses PATH, can take args)
;;process_name=%(program_name)s ; process_name expr (default %(program_name)s)
;;numprocs=1                    ; number of processes copies to start (def 1)
directory=$LOGDIR                ; directory to cwd to before exec (def no cwd)
;;umask=022                     ; umask for process (default None)
;;priority=999                  ; the relative start priority (default 999)
autostart=true                ; start at supervisord start (default: true)
autorestart=true              ; retstart at unexpected quit (default: true)
;;startsecs=10                  ; number of secs prog must stay running (def. 1)
;;startretries=3                ; max # of serial start failures (default 3)
exitcodes=0,2                 ; 'expected' exit codes for process (default 0,2)
;;stopsignal=QUIT               ; signal used to kill process (default TERM)
;;stopwaitsecs=10               ; max num secs to wait b4 SIGKILL (default 10)
;;user=chrism                   ; setuid to this UNIX account to run the program
redirect_stderr=true          ; redirect proc stderr to stdout (default false)
stdout_logfile=$LOGDIR/$PORT.log        ; stdout log path, NONE for none; default AUTO
stdout_logfile_maxbytes=1024MB   ; max # logfile bytes b4 rotation (default 50MB)
stdout_logfile_backups=10     ; # of stdout logfile backups (default 10)
;;stdout_capture_maxbytes=1MB   ; number of bytes in 'capturemode' (default 0)
;;stdout_events_enabled=false   ; emit events on stdout writes (default false)
stderr_logfile=$LOGDIR/$PORT.log        ; stderr log path, NONE for none; default AUTO
;;stderr_logfile_maxbytes=1MB   ; max # logfile bytes b4 rotation (default 50MB)
;;stderr_logfile_backups=10     ; # of stderr logfile backups (default 10)
;;stderr_capture_maxbytes=1MB   ; number of bytes in 'capturemode' (default 0)
;;stderr_events_enabled=false   ; emit events on stderr writes (default false)
;;environment=A=1,B=2           ; process environment additions (def no adds)
;;serverurl=AUTO                ; override serverurl computation (childutils)
;
;; The below sample eventlistener section shows all possible
;; eventlistener subsection values, create one or more 'real'
;; eventlistener: sections to be able to handle event notifications
;; sent by supervisor.
;
;
EOF
done