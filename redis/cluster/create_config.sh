#!/bin/bash
# author : Jalright
# create : 2021-05-27 create redis cluster config
CONFIGDIR="/data/redis/conf"
DATADIR="/data/redis/data"
PORTS=(7001 7002 7003 7004 7005 7006 7007 7008 7009)
PASSWORD="OxYLZdfP1dvBpice"
if [ "${PASSWORD}" == "" ]; then
    # if password is not set , create random password
    arr=(a b c d e f g h i g k l m n o p q r s t u v w x y z
        A B C D E F G H I G K L M N O P Q R S T U V W X Y Z
        / . 0 1 2 3 4 5 6 7 8 9)
    for ((i = 0; i < 16; i++)); do
        PASSWORD=${PASSWORD}${arr[$RANDOM % ${#arr[@]}]}
    done
fi
mkdir -p ${DATADIR} ${CONFIGDIR}
echo $PORTS
for ((i = 0; i < ${#PORTS[@]}; i++)); do
    PORT=${PORTS[${i}]}
    cat >${CONFIGDIR}/${PORT}.conf <<EOF
bind 0.0.0.0
protected-mode yes
port ${PORT}
daemonize yes
pidfile ${DATADIR}/redis_${PORT}.pid
loglevel notice
logfile "${DATADIR}/${PORT}.log"
databases 16
cluster-enabled yes
cluster-config-file cluster_node_${PORT}.conf
cluster-node-timeout 5000
slowlog-log-slower-than 10000
slowlog-max-len 128
dbfilename dump_${PORT}.rdb
dir ${DATADIR}
maxmemory 4gb
appendonly yes
appendfilename "appendonly_${PORT}.aof"
appendfsync everysec
requirepass ${PASSWORD}
masterauth ${PASSWORD}
EOF
done
