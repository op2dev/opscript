# 部署 redis 相关脚本

安装只要安装一个，就可以配置多个实例，因此安装和配置分开执行。

## 安装脚本

```bash
bash ./install_redis_centos.sh
```

## 配置脚本

如果需要自定义路径，可以进入脚本修改

```bash
bash ./config_redis.sh 6379
```

> 如果需要多个实例，多次执行脚本就可以了。
