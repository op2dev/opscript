#!/bin/bash

cd ${HOME}

version="3.4.14"
wget -c https://mirrors.huaweicloud.com/apache/zookeeper/zookeeper-${version}/zookeeper-${version}.tar.gz || exit 1

tar zxvf zookeeper-${version}.tar.gz

mv zookeeper-${version} /opt/

cd /opt/

ln -s zookeeper-${version} zookeeper

mkdir -p /data/zookeeper

cat >/opt/zookeeper/conf/zoo.cfg <<EOF
tickTime=2000
initLimit=10
syncLimit=5
dataDir=/data/zookeeper
clientPort=2181
autopurge.snapRetainCount=3
autopurge.purgeInterval=1
#server.1=192.168.1.101:2888:3888
#server.2=192.168.1.102:2888:3888
#erver.3=192.168.1.103:2888:3888
EOF
