#!/bin/bash

version="7.6.1"

cd $HOME

package_url="https://mirrors.huaweicloud.com/elasticsearch/${version}/elasticsearch-${version}-linux-x86_64.tar.gz"

wget -c ${package_url} || exit 1

tar zxvf elasticsearch-${version}-linux-x86_64.tar.gz

mv elasticsearch-${version} /opt/

cd /opt/

ln -sf elasticsearch-${version} elasticsearch

echo vm.max_map_count=262144 >>/etc/sysctl.conf

sysct -p

echo "* soft nofile 65536" >>/etc/security/limits.conf

echo "* hard nofile 131072" >>/etc/security/limits.conf

mkdir -p /data/elasticsearch/data

mkdir -p /data/elasticsearch/logs

useradd elasticsearch

chown -v elasticsearch:elasticsearch /opt/elasticsearch /opt/elasticsearch-${version} /data/elasticsearch/ -R
