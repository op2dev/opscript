#!/bin/bash

version="7.6.1"

package_url="https://mirrors.huaweicloud.com/kibana/${version}/kibana-${version}-linux-x86_64.tar.gz"

wget -c ${package_url} || exit 1

tar zxvf kibana-${version}-linux-x86_64.tar.gz

mv kibana-${version}-linux-x86_64 /opt/

cd /opt

ln -sf kibana-${version}-linux-x86_64 kibana
