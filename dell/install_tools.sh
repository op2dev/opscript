#!/bin/bash
# auth: Jalright
# date: 2021-05-19
# dell服务器安装dell工具，支持CentOS操作系统
#参考: http://linux.dell.com/repo/hardware/latest/
wget -q -O - http://linux.dell.com/repo/hardware/latest/bootstrap.cgi | bash

yum -y install srvadmin-all
# 启动srvadmin服务
/opt/dell/srvadmin/sbin/srvadmin-services.sh enable
/opt/dell/srvadmin/sbin/srvadmin-services.sh restart