#!/bin/bash
# Author : Jalright
# Date : 2025-01-23

package="jdk-8u202-linux-x64.tar.gz"
source_dir="jdk1.8.0_202"

tar zxvf $tar_file || exit 1

mv $source_dir /opt/jdk || exit 1

if grep 'export JAVA_HOME=/opt/jdk' /etc/profile >>/dev/null 2>&1; then
    echo "already set JAVA_HOME"
else
    echo 'export JAVA_HOME=/opt/jdk' >>/etc/profile
    echo 'export PATH=${JAVA_HOME}/bin:$PATH' >>/etc/profile
fi

source /etc/profile

java -version
