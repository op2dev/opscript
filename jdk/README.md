# 安装 jdk

需要先到 oracle 官网下载对应的压缩包，放到跟脚本同一个目录，然后执行脚本即可。

## jdk 归档版本

直接到oracle官网直接查找，一般都是展示最新版本的JDK，如果需要之前的版本就需要当归档页面去下载。

归档地址：[https://repo.huaweicloud.com/java/jdk/8u202-b08/](https://repo.huaweicloud.com/java/jdk/8u202-b08/)"

## 修改对应的包的名称

``` bash
package="jdk-8u202-linux-x64.tar.gz"
source_dir="jdk1.8.0_202"
```

