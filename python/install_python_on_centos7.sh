#!/bin/bash
# author : Jalright
# date : 2021-05-09
# update date : 2021-06-04

VERSION='3.10.7'

yum -y install xz tar gcc make tk-devel wget sqlite-devel zlib-devel  readline-devel openssl-devel curl-devel tk-devel gdbm-devel xz-devel bzip2-devel libffi-devel

cd /root/

wget -c https://mirrors.huaweicloud.com/python/${VERSION}/Python-${VERSION}.tar.xz

tar xvf Python-${VERSION}.tar.xz

cd Python-${VERSION}

./configure --prefix=/opt/python-${VERSION}

make

make install

echo "export PATH=/opt/python-${VERSION}/bin:\$PATH" >>/etc/profile

mkdir -p ~/.pip

# 设置镜像
cat > ~/.pip/pip.conf <<EOF
[global]
index-url = https://mirrors.aliyun.com/pypi/simple/

[install]
trusted-host=mirrors.aliyun.com
EOF

source /etc/profile

python3 -V
