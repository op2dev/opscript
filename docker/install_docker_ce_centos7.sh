#!/bin/bash
# Author : Jalright
# date : 2020-03-02
# History : 2020-03-06:
#           1. 完善安装流程

## 1、检查，并删除现有的docker及相关组件
yum remove docker docker-common docker-selinux docker-engine

## 2、安装相关依赖
yum install -y yum-utils device-mapper-persistent-data lvm2

## 3、添加安装源,提供阿里及华为两种方式，任选其一
# 3.1、添加华为云的yum源
wget -O /etc/yum.repos.d/docker-ce.repo https://mirrors.huaweicloud.com/docker-ce/linux/centos/docker-ce.repo
# 替换华为云的镜像地址
sed -i 's+download.docker.com+mirrors.huaweicloud.com/docker-ce+' /etc/yum.repos.d/docker-ce.repo
# 3.2、使用阿里云
#yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

## 4、更新yum源，并安装最新的docker-ce
yum makecache fast
yum install docker-ce -y

## 5、启动docker，并设置开机启动
service docker start
systenctl enabled docker

## 6、其他（如下内容按需手动执行或修改脚本执行）
# 6.1、安装指定版本docker-ce
#vim /etc/yum.repos.d/docker-ce.repo
#将 [docker-ce-test] 下方的 enabled=0 修改为 enabled=1
# 6.2、查找docker-ce版本：
#yum list docker-ce.x86_64 --showduplicates | sort -r
# 6.3、安装指定版本：
#yum -y install docker-ce-[VERSION]
#例如：yum -y install docker-ce-17.03.0.ce.1-1.el7.centos)
# 6.4、阿里云网络环境下，如经典网络、VPC网络添加yum源
# 经典网络：
# sudo yum-config-manager --add-repo http://mirrors.aliyuncs.com/docker-ce/linux/centos/docker-ce.repo
# VPC网络：
# sudo yum-config-manager --add-repo http://mirrors.could.aliyuncs.com/docker-ce/linux/centos/docker-ce.repo
# 6.5、docker镜像加速
# 镜像加速地址请登入https://console.aliyun.com/  -->  容器镜像服务  -->  配置registry密码  -->  镜像中心  -->  镜像加速器  -->  Centos  --> 拷贝对应的加速器地址替换下方registry-mirrors
#mkdir -p /etc/docker
#tee /etc/docker/daemon.json <<-'EOF'
# {
#  "registry-mirrors": ["https://xxxxxx.mirror.aliyuncs.com"]
#}
#EOF
# 6.5、重启生效
#systemctl daemon-reload
#systemctl restart docker
