#!/bin/bash

cat >Dockerfile <<EOF
From alpine:3.15.0
ADD https://download.redis.io/releases/redis-6.2.6.tar.gz /
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories && apk add make gcc g++ linux-headers tar && tar zxvf redis-6.2.6.tar.gz && cd redis-6.2.6 && make && make install && apk del make gcc g++ linux-headers tar && rm -rfv /redis-6.2.6 /redis-6.2.6.tar.gz
ADD entrypoint.sh /
ENTRYPOINT [ "/bin/sh","/entrypoint.sh" ]
EOF

docker build -t redis:v6.2.6 .
