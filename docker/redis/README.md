# redis镜像制作

采用apline基础镜像，通过编译源码进行安装。

## 使用

```bash
docker run -itd -e PASSWORD=123 redis:v6.2.6
```

*支持变量：*

- PASSWORD：密码
- PORT：端口
- MAXMEMORY:最大内存
- PROMTECTEDMODE：保护模式（默认是：no）
- DATADIR:数据目录
