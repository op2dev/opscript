#!/bin/sh
set -e

port=6379
if [ "$PORT" != "" ] ; then
    port=$PORT
fi
data_dir=/data/redis/data
if [ "$DATADIR" != "" ] ; then
    data_dir=$DATADIR
fi
mkdir -p $data_dir
password=""
if [ "$PASSWORD" != "" ] ; then
    password=$PASSWORD
fi
maxmemory=0
if [ "$MAXMEMORY" != "" ] ; then
    maxmemory=$MAXMEMORY
fi
protected_mode="no"
if [ "$PROTECTEDMODE" != "" ] ; then
    protected_mode=$PROTECTEDMODE
fi
if [[ "$PASSWORD" == "" && "$protected_mode" == "yes" ]]; then
    echo "protected-mode need password!!!!"
    exit 1
fi
cat >/etc/redis.conf <<EOF
bind 0.0.0.0
protected-mode ${protected_mode}
port ${port}
daemonize no
pidfile ${data_dir}/redis_${port}.pid
loglevel notice
logfile "${data_dir}/${port}.log"
databases 16
dbfilename dump_${port}.rdb
dir ${data_dir}
maxmemory $maxmemory
appendonly yes
appendfilename "appendonly_${port}.aof"
appendfsync everysec
requirepass "${password}"
EOF
/usr/local/bin/redis-server /etc/redis.conf