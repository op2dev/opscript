#!/bin/bash

VERSION="v3.12"
SUBVERSION="3.12.1"
# 下载连接
package_url="https://mirrors.aliyun.com/alpine/${VERSION}/releases/x86_64/alpine-minirootfs-${SUBVERSION}-x86_64.tar.gz"

docker import ${package_url} alpine:${SUBVERSION}
