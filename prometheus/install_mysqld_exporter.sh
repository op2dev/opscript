#!/bin/bash

VERSION="0.12.1"

wget  -t 100  -c  https://github.com/prometheus/mysqld_exporter/releases/download/v${VERSION}/mysqld_exporter-${VERSION}.linux-amd64.tar.gz

if [ ! -e mysqld_exporter-${VERSION}.linux-amd64.tar.gz ]
then
    echo "安装包下载失败"
    exit 1
fi
tar xvfz mysqld_exporter-${VERSION}.linux-amd64.tar.gz -C /opt/
cd /opt
ln -s mysqld_exporter-${VERSION}.linux-amd64  mysqld_exporter
cat > /etc/systemd/system/mysqld_exporter.service <<EOF

[Unit]
Description=mysqld_exporter
After=network.target

[Service]
Type=simple
WorkingDirectory=/opt/mysqld_exporter
ExecStart=/opt/mysqld_exporter/mysqld_exporter --config.my-cnf="/opt/mysqld_exporter/.my.cnf"
LimitNOFILE=65536
PrivateTmp=true
RestartSec=2
StartLimitInterval=0
Restart=always

[Install]
WantedBy=multi-user.target
EOF
```
