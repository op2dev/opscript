#!/bin/bash

VERSION="1.0.1"

wget  -t 100  -c  https://github.com/prometheus/node_exporter/releases/download/v${VERSION}/node_exporter-${VERSION}.linux-amd64.tar.gz

if [ ! -e node_exporter-${VERSION}.linux-amd64.tar.gz ]
then
    echo "安装包下载失败"
    exit 1
fi
tar xvfz node_exporter-${VERSION}.linux-amd64.tar.gz -C /opt/
cd /opt
ln -s node_exporter-${VERSION}.linux-amd64  node_exporter
cat > /etc/systemd/system/node_exporter.service <<EOF

[Unit]
Description=node_exporter
After=network.target

[Service]
Type=simple
WorkingDirectory=/opt/node_exporter
ExecStart=/opt/node_exporter/node_exporter
LimitNOFILE=65536
PrivateTmp=true
RestartSec=2
StartLimitInterval=0
Restart=always

[Install]
WantedBy=multi-user.target
EOF


systemctl daemon-reload

systemctl enable node_exporter
systemctl start node_exporter