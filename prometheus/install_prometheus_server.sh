#!/bin/bash

VERSION="2.23.0"

wget -t 100  -c https://github.com/prometheus/prometheus/releases/download/v${VERSION}/prometheus-${VERSION}.linux-amd64.tar.gz

if [ ! -e prometheus-${VERSION}.linux-amd64.tar.gz ]
then
    echo "安装包下载失败"
    exit 1
fi


tar zxvf  prometheus-${VERSION}.linux-amd64.tar.gz -C /opt/

cd /opt/

ln -s prometheus-${VERSION}.linux-amd64 prometheus


cat > /etc/systemd/system/prometheus.service <<EOF
[Unit]
Description=prometheus
After=network.target

[Service]
Type=simple
WorkingDirectory=/opt/prometheus/prometheus
ExecStart=/opt/prometheus/prometheus --config.file="/opt/prometheus/prometheus.yml"
LimitNOFILE=65536
PrivateTmp=true
RestartSec=2
StartLimitInterval=0
Restart=always

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload

systemctl enable prometheus
systemctl start prometheus