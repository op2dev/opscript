#!/bin/bash

VERSION="1.15.0"

wget  -t 100  -c  https://github.com/oliver006/redis_exporter/releases/download/v${VERSION}/redis_exporter-${VERSION}.linux-amd64.tar.gz

if [ ! -e redis_exporter-${VERSION}.linux-amd64.tar.gz ]
then
    echo "安装包下载失败"
    exit 1
fi
tar xvfz redis_exporter-${VERSION}.linux-amd64.tar.gz -C /opt/
cd /opt
ln -s redis_exporter-${VERSION}.linux-amd64  redis_exporter
cat > /etc/systemd/system/redis_exporter.service <<EOF

[Unit]
Description=redis_exporter
After=network.target

[Service]
Type=simple
WorkingDirectory=/opt/redis_exporter
ExecStart=/opt/redis_exporter/redis_exporter -redis.addr redis://localhost:6379
LimitNOFILE=65536
PrivateTmp=true
RestartSec=2
StartLimitInterval=0
Restart=always

[Install]
WantedBy=multi-user.target
EOF


systemctl daemon-reload

systemctl enable redis_exporter
systemctl start redis_exporter