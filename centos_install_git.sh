#!/bin/bash
wget -c https://mirrors.edge.kernel.org/pub/software/scm/git/git-2.40.0.tar.xz
yum -y install gcc  openssl-devel libcurl-devel expat-devel wget
yum -y remove git
make prefix=/usr/local/git all
make prefix=/usr/local/git install
echo "export PATH=$PATH:/usr/local/git/bin" >> /etc/profile
source /etc/profile
git --version
