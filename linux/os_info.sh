#!/bin/bash
export LANG=en_US.UTF-8
# 定义颜色代码
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
CYAN='\033[0;36m'
NC='\033[0m' # 恢复默认颜色

# 获取系统信息函数
get_system_info() {
    echo -e "${BLUE}========== 系统信息 ==========${NC}"
    echo -e "${GREEN}主机名:${NC}\t\t $(hostname)"
    echo -e "${GREEN}操作系统:${NC}\t $(grep PRETTY_NAME /etc/os-release | cut -d= -f2 | tr -d '\"')"
    echo -e "${GREEN}内核版本:${NC}\t $(uname -r)"
    echo -e "${GREEN}系统架构:${NC}\t $(uname -m)"
    echo -e "${GREEN}启动时间:${NC}\t $(uptime -s)"
    echo -e "${GREEN}运行时间:${NC}\t $(uptime -p)"
}

# 获取CPU信息
get_cpu_info() {
    echo -e "\n${CYAN}========== CPU信息 ==========${NC}"
    echo -e "${GREEN}CPU型号:${NC}\t $(lscpu | grep -E 'Model name|型号名称' | cut -d: -f2 | xargs)"
    echo -e "${GREEN}核心数量:${NC}\t $(nproc) 核"
    echo -e "${GREEN}CPU频率:${NC}\t $(lscpu | grep -E 'CPU .* MHz' | awk '{print $NF " MHz"}'| head -n 1)"
    echo -e "${GREEN}负载情况:${NC}\t $(uptime | awk -F 'load average: ' '{print $2}')"
}

# 获取内存信息
get_memory_info() {
    echo -e "\n${YELLOW}========== 内存信息 ==========${NC}"
    free -h | awk '
        /Mem:/ {printf "总内存: \t %s\n已用内存: \t %s\n剩余内存: \t %s\n", $2, $3, $4}
        /Swap:/ {printf "Swap总量: \t %s\nSwap已用: \t %s\nSwap剩余: \t %s\n", $2, $3, $4}
    '
}

# 获取磁盘信息
get_disk_info() {
    echo -e "\n${RED}========== 磁盘信息 ==========${NC}"
    df -h | grep -vE '^Filesystem|tmpfs|overlay' | awk '
    {
        printf "%-20s %-10s %-10s %-10s %s\n", 
        $1, $2, $3, $4, $6
    }' | column -t -N "文件系统,总大小,已用,可用,挂载点"
}

# 获取网络信息
get_network_info() {
    echo -e "\n${BLUE}========== 网络信息 ==========${NC}"
    echo -e "${GREEN}IP地址:${NC}\t $(hostname -i | awk '{print $1}')"
    echo -e "${GREEN}公网IP:${NC}\t $(curl -s opcai.top/ip)"
    echo -e "${GREEN}网络接口:"
    ip -o link show | awk -F': ' '{print "  ➤ " $2}'
}

# 主函数
main() {
    clear
    echo -e "${BLUE}================================"
    echo -e "      Linux 主机信息报告"
    echo -e "================================${NC}"
    
    get_system_info
    get_cpu_info
    get_memory_info
    get_disk_info
    get_network_info
    
    echo -e "\n${GREEN}报告生成时间:${NC} $(date "+%Y-%m-%d %H:%M:%S")"
}

# 执行主函数
main
